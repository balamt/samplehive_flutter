##Sample Hive Flutter example
Creating Hive Flutter sample following the instruction from youtube channel 
**["Waga Odongo"](https://www.youtube.com/channel/UCczwimlAfdVmSHCCW7jt8DA)** - Actual [Playlist link](https://www.youtube.com/watch?v=MNuhoZrlhgw&list=PLdBY1aYxSpPUsbJqPH2rRyaIG1aEUjwlD) for the entire series.

###Highlights of the Course/Guided Video:
1. You can learn the basics of Flutter
2. Able to understand the Routes (a bit of knowledge is shared in the video)
3. Easy to understand the Hive, Adapters (Generated), and the Providers for data
4. Creating widgets (Few widgets created in the videos can be refactored to reuse the existing widgets, but not following those aspects in the guide)
5. Loggers, how to introduce the loggers
6. show custome dialog box
7. showing toast (can be customized as well)
8. Action buttons/icons in AppBar
-------------------------------------

Thanks 
 **["Waga Odongo"](https://www.youtube.com/channel/UCczwimlAfdVmSHCCW7jt8DA)** for the guide.