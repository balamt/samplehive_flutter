import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/transactionData.dart';
import 'package:sample_hive/widgets/transactionTile.dart';

class TransactionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (context, index) {
      return TransactionTile(
        tileIndex: index,
      );
    },
      itemCount: Provider.of<TransactionData>(context).transactionsCount,
      padding: EdgeInsets.all(4),
    );
  }
}
