import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


void toastWidget(msg){
  Fluttertoast.showToast(msg: msg,
  toastLength: Toast.LENGTH_SHORT,
  gravity: ToastGravity.CENTER,
  timeInSecForIosWeb: 3,
  backgroundColor: Colors.blueGrey,
    textColor: Colors.amberAccent,
    fontSize: 16.0
  );
}
