import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/model/transaction.dart';
import 'package:sample_hive/pages/transactionView.dart';
import 'package:sample_hive/transactionData.dart';

class TransactionTile extends StatelessWidget {
  final int tileIndex;

  const TransactionTile({Key key, this.tileIndex});

  @override
  Widget build(BuildContext context) {
    return Consumer<TransactionData>(
        builder: (context, transactionData, child) {
      Transaction _currentTransaction =
          transactionData.getTransaction(tileIndex);

      return Container(
        decoration: BoxDecoration(
          color: tileIndex % 2 == 0 ? Colors.black54 : Colors.grey,
        ),
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.indigo,
            foregroundColor: Colors.white,
            child: Text(
              _currentTransaction.category
                  .substring(0, 1)
                  .toUpperCase(),
              style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            ),
          ),
          title: Text(
            _currentTransaction.category ?? "Catetory",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30,
              color: Colors.black,
            ),
          ),
          subtitle: Text(
            _currentTransaction.paymentMode ?? "Payment mode",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 20,
              color: Colors.black,
            ),
          ),
          onTap: () {
            Provider.of<TransactionData>(context, listen: false)
                .setSelectedTransaction(_currentTransaction.key);
            Navigator.push(context, MaterialPageRoute(
              builder: (context){
                return TransactionView();
              }
            ));
          },
        ),
      );
    });
  }
}
