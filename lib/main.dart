import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/model/transaction.dart';
import 'package:sample_hive/pages/addTransactionPage.dart';
import 'package:sample_hive/pages/transactionList.dart';
import 'package:sample_hive/transactionData.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() {
  Hive.registerAdapter(TransactionAdapter());

  runApp(SampleHiveMainApp());
}

Future _initHive() async {
  var docDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(docDir.path);
}

class SampleHiveMainApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TransactionData(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Hive Sample App',
        theme: ThemeData(
          primaryColor: Colors.lightBlueAccent,
        ),
        initialRoute: "/",
        routes: {
          "/": (context) => FutureBuilder(
                future: _initHive(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.error != null) {
                      print(snapshot.error);
                      return Scaffold(
                        body: Center(
                          child: Text(
                            "Error establishing conenction with Hive Box",
                            style: TextStyle(
                              color: Colors.white,
                              backgroundColor: Colors.red,
                            ),
                          ),
                        ),
                      );
                    }else{
                      print(snapshot.error);
                      return TransactionListPage();
                    }
                  }else{
                    return Scaffold();
                  }
                },
              ),
          "/AddTransactionPage": (context) => AddTransactionPage(),
        },
      ),
    );
  }
}

