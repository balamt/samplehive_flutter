import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/model/transaction.dart';
import 'package:sample_hive/pages/editTransactionPage.dart';
import 'package:sample_hive/transactionData.dart';

import '../utils.dart';

class TransactionView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void _deleteConfirmation(currentTransaction) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Text(
              "Are you sure you want to delete?",
              style: TextStyle(
                fontSize: 26,
                color: Colors.black,
              ),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text("You are about to delete${currentTransaction.category}"),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text("This action cannot be undone"),
                ],
              ),
            ),
            actions: [
              FlatButton(
                onPressed: () {
                  Log.i("Deleting ${currentTransaction.category}");
                  Provider.of<TransactionData>(context, listen: false)
                      .removeTransaction(currentTransaction.key);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName(Navigator.defaultRouteName),
                  );
                },
                child: Text(
                  "DELETE",
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  Log.i("Cancelled ${currentTransaction.category}");
                  Navigator.of(context).pop();
                },
                child: Text(
                  "Cancel",
                  style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          );
        },
      );
    }

    return Consumer<TransactionData>(
      builder: (context, transactionData, child) {
        Transaction _currentTransaction =
            transactionData.getSelectedTransaction();

        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.lightBlueAccent,
            elevation: 12.0,
            title: Text(
              _currentTransaction?.transactionType,
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.create,
                    color: Colors.deepOrange,
                  ),
                  onPressed: () {
                    Log.i("Edit");
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return EditTransactionPage(
                        currentTransaction: _currentTransaction,
                      );
                    }));
                  }),
              IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    Log.i("Delete Button");
                    _deleteConfirmation(_currentTransaction);
                  }),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.all(10),
            child: Center(
              child: Column(
                children: [
                  Container(
                    height: 36.0,
                    color: Colors.grey,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Category",
                          style: TextStyle(
                            fontSize: 28,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          _currentTransaction?.category,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
