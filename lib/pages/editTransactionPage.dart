import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/model/transaction.dart';
import 'package:sample_hive/transactionData.dart';
import 'package:sample_hive/widgets/toast.dart';

class EditTransactionPage extends StatefulWidget {
  final Transaction currentTransaction;

  const EditTransactionPage({Key key, this.currentTransaction});

  @override
  _EditTransactionPageState createState() => _EditTransactionPageState();
}

class _EditTransactionPageState extends State<EditTransactionPage> {
  String category, transactionType, paymentMode;
  int transactionId;
  DateTime createdOn = DateTime.now();
  var paymentModes = {'ACCOUNT', 'CARD', 'CASH'};

  void _editTransaction(context) {
    if (this.category == null) {
      toastWidget("Category is Empty, Cannot be empty");
      return;
    }

    if (this.paymentMode != null &&
        paymentModes.contains(this.paymentMode.trim().toUpperCase())) {
      toastWidget("paymentMode has to be ACCOUNT, CARD, CASH");
      return;
    }

    Provider.of<TransactionData>(context, listen: false).editTransaction(
        transaction: Transaction(
          transactionId: transactionId,
          transactionType: transactionType,
          category: category,
          createdOn: createdOn ?? DateTime.now(),
          paymentMode: paymentMode,
        ),
        key: widget.currentTransaction.key);

    Navigator.pop(context);
  }

  final TextEditingController _categoryTextController = TextEditingController();
  final TextEditingController _transactionTypeTextController =
      TextEditingController();
  final TextEditingController _transactionIdController =
      TextEditingController();
  final TextEditingController _paymentModeTextController =
      TextEditingController();

  @override
  void initState() {
    _categoryTextController.text = widget.currentTransaction.category;
    category = widget.currentTransaction.category;

    _transactionTypeTextController.text =
        widget.currentTransaction.transactionType;
    transactionType = widget.currentTransaction.transactionType;

    _transactionIdController.text =
        widget.currentTransaction.transactionId.toString();
    transactionId = widget.currentTransaction.transactionId;

    _paymentModeTextController.text = widget.currentTransaction.paymentMode;
    paymentMode = widget.currentTransaction.paymentMode;

    createdOn = widget.currentTransaction.createdOn;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        elevation: 16.0,
        title: Text(
          "Edit Transaction -  ${widget.currentTransaction.category}",
          style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.save,
                color: Colors.orange,
                size: 24.0,
              ),
              onPressed: () {
                _editTransaction(context);
              }),
        ],
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextField(
                autofocus: false,
                controller: _categoryTextController,
                decoration: InputDecoration(hintText: "Category"),
                onChanged: (cat) {
                  setState(() {
                    category = cat;
                  });
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                autofocus: false,
                controller: _transactionTypeTextController,
                decoration: InputDecoration(hintText: "Transaction Type"),
                onChanged: (tType) {
                  setState(() {
                    transactionType = tType;
                  });
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                autofocus: false,
                controller: _transactionIdController,
                keyboardType: TextInputType.numberWithOptions(decimal: false),
                decoration: InputDecoration(hintText: "Transaction Id"),
                onChanged: (tId) {
                  setState(() {
                    transactionId = int.parse(tId);
                  });
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                autofocus: false,
                controller: _paymentModeTextController,
                decoration: InputDecoration(hintText: "Payment Mode"),
                onChanged: (pMode) {
                  setState(() {
                    paymentMode = pMode;
                  });
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                children: [
                  Text(
                    "Created On",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.calendar_today),
                    tooltip: "Tap to show calendar to choose Created On",
                    onPressed: () async {
                      final DateTime selectedDate = await showDatePicker(
                        context: context,
                        initialDate: createdOn ?? DateTime.now(),
                        firstDate: DateTime(2019),
                        lastDate: DateTime.now(),
                      );
                      if (selectedDate != null) {
                        setState(() {
                          createdOn = selectedDate;
                        });
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
