import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/model/transaction.dart';
import 'package:sample_hive/transactionData.dart';
import 'package:sample_hive/widgets/toast.dart';

class AddTransactionPage extends StatefulWidget {
  @override
  _AddTransactionPageState createState() => _AddTransactionPageState();
}

class _AddTransactionPageState extends State<AddTransactionPage> {
  String category, transactionType, paymentMode;
  int transactionId;
  DateTime createdOn = DateTime.now();

  _addTransaction(context) {
    if (category == null) {
      toastWidget("Category is empty");
      return;
    }

    if (transactionType == null) {
      toastWidget("Transaction Type is empty");
      return;
    }
    Provider.of<TransactionData>(context, listen: false).addTransaction(
        Transaction(
            category: category,
            transactionId: transactionId,
            transactionType: transactionType,
            paymentMode: paymentMode,
            createdOn: createdOn ?? DateTime.now()));

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlueAccent,
          elevation: 16.0,
          title: Text(
            "Add Transaction",
            style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.save,
                  color: Colors.orange,
                  size: 24.0,
                ),
                onPressed: () {
                  _addTransaction(context);
                }),
          ],
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(4),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextField(
                  autofocus: false,
                  decoration: InputDecoration(hintText: "Category"),
                  onChanged: (cat) {
                    setState(() {
                      category = cat;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  autofocus: false,
                  decoration: InputDecoration(hintText: "Transaction Type"),
                  onChanged: (tType) {
                    setState(() {
                      transactionType = tType;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  autofocus: false,
                  keyboardType: TextInputType.numberWithOptions(decimal: false),
                  decoration: InputDecoration(hintText: "Transaction Id"),
                  onChanged: (tId) {
                    setState(() {
                      transactionId = int.parse(tId);
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  autofocus: false,
                  decoration: InputDecoration(hintText: "Payment Mode"),
                  onChanged: (pMode) {
                    setState(() {
                      paymentMode = pMode;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    Text(
                      "Created On",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.calendar_today),
                      tooltip: "Tap to show calendar to choose Created On",
                      onPressed: () async {
                        final DateTime d = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2019),
                          lastDate: DateTime.now(),
                        );
                        if (d != null) {
                          setState(() {
                            createdOn = d;
                          });
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
