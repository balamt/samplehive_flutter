import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_hive/transactionData.dart';
import 'package:sample_hive/widgets/transactionList.dart';

class TransactionListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<TransactionData>(context, listen: false).getTransactions();

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlueAccent,
          elevation: 12.0,
          title: Text('Transactions',
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
          ),),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(child: Container(
                child: TransactionList(),
              )),
            ],
          ),
        ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightBlueAccent,
        elevation: 12.0,
        tooltip: "Add New Transaction",
        child: Icon(
          Icons.add
        ),
        onPressed: (){
          Navigator.pushNamed(context, "/AddTransactionPage");
        },
      ),
    );
  }
}
