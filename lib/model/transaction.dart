import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:sample_hive/model/user.dart';

part 'transaction.g.dart';

@HiveType(typeId: 0)
class Transaction extends HiveObject {
  @HiveField(0)
  String paymentMode;
  @HiveField(1)
  String transactionType;
  @HiveField(2)
  String category;
  @HiveField(3)
  DateTime createdOn;
  @HiveField(4)
  int transactionId;

  Transaction({
  this.paymentMode = "",
  this.transactionType = "",
  this.category = "",
  this.createdOn,
  this.transactionId = 0,
  });

  Transaction.fromJson(Map<String, dynamic>  map) :
        paymentMode = map['paymentMode']  ?? "",
        transactionType = map['transactionType']  ?? "",
        category = map['category']  ?? "",
        createdOn = map['createdOn']  ?? DateTime.now(),
        transactionId = map['transactionId']  ?? 0;

  Map<String, dynamic> toJson() => {
        'paymentMode': paymentMode,
        'transactionType': transactionType,
        'category': category,
        'createdOn': createdOn,
        'transactionId': transactionId,
      };

  Transaction copyWith({
    String paymentMode,
    String transactionType,
    String category,
    DateTime createdOn,
    User user,
    int transactionId,
  }) {
    return Transaction(
      paymentMode: paymentMode ?? this.paymentMode,
      transactionType: transactionType ?? this.transactionType,
      category: category ?? this.category,
      createdOn: createdOn ?? this.createdOn,
      transactionId: transactionId ?? this.transactionId,
    );
  }
}

