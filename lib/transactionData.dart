import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:sample_hive/model/transaction.dart';
import 'utils.dart';

class TransactionData extends ChangeNotifier{
  static const String _boxName = "transactionBox";
  
  List<Transaction> _transactions = [];
  
  Transaction _selectedTransaction;

  void getTransactions() async {
    var txBox = await Hive.openBox<Transaction>(_boxName);
    _transactions = txBox.values.toList();
    notifyListeners();
  }

  Transaction getTransaction(index){
    return _transactions[index];
  }

  void addTransaction(Transaction transaction) async{
    var txBox = await Hive.openBox<Transaction>(_boxName);
    await txBox.add(transaction);
    _transactions = txBox.values.toList();
    notifyListeners();
  }

  void removeTransaction(key) async{
    var txBox = await Hive.openBox<Transaction>(_boxName);
    await txBox.delete(key);
    _transactions = txBox.values.toList();
    notifyListeners();
    Log.i("Deleted the key " + key.toString());
  }

  void editTransaction({Transaction transaction, int key}) async{
    var txBox = await Hive.openBox<Transaction>(_boxName);
    await txBox.put(key, transaction);
    _transactions = txBox.values.toList();
    _selectedTransaction = txBox.get(key);
    Log.i("Edit the key " + transaction.transactionType);
    notifyListeners();
  }

  void setSelectedTransaction(key) async{
    var txBox = await Hive.openBox<Transaction>(_boxName);
    _selectedTransaction = txBox.get(key);
    notifyListeners();
  }

  Transaction getSelectedTransaction() {
    return _selectedTransaction;
  }

  int get transactionsCount {
    return _transactions.length;
  }

}